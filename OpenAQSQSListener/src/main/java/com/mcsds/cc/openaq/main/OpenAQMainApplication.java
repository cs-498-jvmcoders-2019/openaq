package com.mcsds.cc.openaq.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

//“convention over configuration.”

@SpringBootApplication(exclude = {ContextRegionProviderAutoConfiguration.class })
@ImportResource("classpath:aws-config.xml")
@ComponentScan("com.mcsds.cc.openaq")
public class OpenAQMainApplication {
	public static void main(String[] args) {
		SpringApplication.run(OpenAQMainApplication.class, args);
	}
}
