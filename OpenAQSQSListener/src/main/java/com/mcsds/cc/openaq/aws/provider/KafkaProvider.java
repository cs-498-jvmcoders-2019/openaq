package com.mcsds.cc.openaq.aws.provider;

import org.apache.kafka.clients.producer.Producer;

public interface KafkaProvider {
	
	Producer<String, byte[]> createProducer();
}
