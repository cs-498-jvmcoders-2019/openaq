package com.mcsds.cc.openaq.aws.provider;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.mcsds.cc.openaq.constants.DataConstants;

@Component
public class OpenAQKafkaProvider implements KafkaProvider {

	@Bean(name="kafkaProducer")
	public Producer<String, byte[]> createProducer() {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, DataConstants.BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, DataConstants.DEFAULT_OPENAQ_BUCKET_ARN);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
		props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, "60000000");
		return new KafkaProducer<String, byte[]>(props);
	}
	
	
}
