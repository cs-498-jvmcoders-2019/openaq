package com.mcsds.cc.openaq.constants;

public class DataConstants {

	public static final String DEFAULT_IP = "127.0.0.1";
	public static final String REMOTE_MAC_IP = "192.168.1.191"; //24 running kafka, zookeeper, hbase, hdfs etc
	public static final String REMOTE_WIN_IP_1 = "192.168.1.192"; //24 running kafka, zookeeper, hbase, hdfs etc 
	public static final String REMOTE_WIN_IP_2 = "192.168.1.77"; //24 running kafka, zookeeper alone
	public final static String TOPIC = "openaq-stream-in-data-topic"; // this is topic where .json file will be pushed.
	public final static String BOOTSTRAP_SERVERS = "192.168.1.191:9092"; // Change your kafka server:port accordingly.
	
	public static final String DEFAULT_OPENAQ_BUCKET_ARN = "arn:aws:s3:::openaq-fetches";
	public static final String DEFAULT_OPENAQ_NEW_MEAS_ARN = "arn:aws:sns:us-east-1:470049585876:OPENAQ_NEW_MEASUREMENT";
	public static final String DEFAULT_OPENAQ_NEW_FETCH_ARN = "arn:aws:sns:us-east-1:470049585876:NewFetchObject";

	public static final String DEFAULT_OPENAQ_BUCKET_NAME = "openaq-fetches";
	public static final String DEFAULT_OPENAQ_NEW_MEAS_NAME = "OPENAQ_NEW_MEASUREMENT";
	public static final String DEFAULT_OPENAQ_NEW_FETCH_NAME = "NewFetchObject";
	
	private enum DATA_TYPE {
		DAILY("daily"),
		REALTIME("realtime"),
		REALTIME_GZ("realtime-gzipped"),
		TEST_REALTIME("test-realtime"),
		TEST_REAL_EVENTS("test-realtime-events"),
		TEST_REAL_GZ("test-realtime-gzip");
		
		private final String dataType;
		DATA_TYPE (String dataType) {
			this.dataType = dataType;
		}
		
		public String getDataType() {
			return dataType;
		}
	}
}
