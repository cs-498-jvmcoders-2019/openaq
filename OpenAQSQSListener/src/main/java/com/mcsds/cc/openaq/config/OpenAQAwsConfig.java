package com.mcsds.cc.openaq.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.core.NotificationMessagingTemplate;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sqs.AmazonSQSAsync;

@Configuration
public class OpenAQAwsConfig {

	private static final Logger logger = LoggerFactory.getLogger(OpenAQAwsConfig.class);

	@Bean
	public QueueMessagingTemplate queueMessagingTemplate(AmazonSQSAsync amazonSQSAsync) {
		logger.info("\nBuilding amazonSQS bean...\n");
		return new QueueMessagingTemplate(amazonSQSAsync);
	}

	@Bean
	public NotificationMessagingTemplate notificationMessagingTemplate(AmazonSNS amazonSNS) {
		logger.info("\nBuilding amazonSNS bean...\n");
		return new NotificationMessagingTemplate(amazonSNS);
	}

	/*
	 * @Bean public AmazonSQSAsync amazonSQS(@Value("us-east-1") String awsRegion) {
	 * System.out.println("\nBuilding amazonSQS bean...\n"); AmazonSQSAsync
	 * theAsyncSqs = null; try { theAsyncSqs =
	 * AmazonSQSAsyncClientBuilder.standard() .withRegion(awsRegion)
	 * .withCredentials(new EC2ContainerCredentialsProviderWrapper()) .build(); }
	 * catch (Exception ex) { System.out.println("Error: \n" + ex.toString() + "\n"
	 * + ex.getStackTrace() + "\n"); } return theAsyncSqs; }
	 * 
	 * @Bean public SimpleMessageListenerContainerFactory
	 * simpleMessageListenerContainerFactory(AmazonSQSAsync amazonSqs) {
	 * SimpleMessageListenerContainerFactory factory = new
	 * SimpleMessageListenerContainerFactory(); factory.setAmazonSqs(amazonSqs);
	 * factory.setMaxNumberOfMessages(5); return factory; }
	 * 
	 * @Bean public QueueMessageHandler queueMessageHandler(AmazonSQSAsync
	 * amazonSQSAsync) { QueueMessageHandlerFactory factory = new
	 * QueueMessageHandlerFactory(); factory.setAmazonSqs(amazonSQSAsync); return
	 * factory.createQueueMessageHandler(); }
	 * 
	 * @Bean public SimpleMessageListenerContainer simpleMessageListenerContainer(
	 * SimpleMessageListenerContainerFactory simpleMessageListenerContainerFactory,
	 * QueueMessageHandler queueMessageHandler) { SimpleMessageListenerContainer
	 * container =
	 * simpleMessageListenerContainerFactory.createSimpleMessageListenerContainer();
	 * container.setMessageHandler(queueMessageHandler);
	 * container.setWaitTimeOut(20); return container; }
	 */

}