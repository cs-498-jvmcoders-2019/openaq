package com.mcsds.cc.openaq.aws.provider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;

/*
 * S3 Bucket manual download options...
 * Option 1. aws cli pull command : aws s3 cp s3://openaq-fetches . --recursive
 * Option 2. aws s3 sync s3://openaq-fetches .
 * Option 3. aws s3 sync s3://openaq-fetches/realtime realtime/.
 * 
 * Note : Option 3 is better since we are concerned only with realtime data. 
 * 
 */
@Component
public class OpenAQDataProvider implements DataProvider {

	private Logger logger = Logger.getLogger(this.getClass());

	@Value("us-east-1")
	public String region;

	@Value("openaq-fetches")
	public String bucket;

	@Value("realtime")
	public String filter;

	@Value("realtime")
	public String key;

	@Autowired
	public ResourceLoader resourceLoader;

	@Autowired
	public AWSCredentials credentials;

	// S3 ...
	@Bean
	public AmazonS3 s3Initializer() {
		AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(region).build();
		return amazonS3;
	}

	@Bean
	public Resource s3ResourceLoader() {
		StringBuilder resourceVal = new StringBuilder().append("s3://").append(bucket).append("/").append(filter);
		Resource resource = this.resourceLoader.getResource(resourceVal.toString());
		return resource;
	}

	public AmazonSQS sqsInitializer() {
		// TODO Auto-generated method stub
		return null;
	}

}
