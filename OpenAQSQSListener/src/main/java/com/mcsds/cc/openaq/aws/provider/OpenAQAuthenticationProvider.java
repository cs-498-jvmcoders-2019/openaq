package com.mcsds.cc.openaq.aws.provider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;

@Configuration
public class OpenAQAuthenticationProvider implements AuthProvider {
	private Logger logger = Logger.getLogger(this.getClass());
	
	static final String DEFAULT_VAL = "default";
	
	@Bean
	@Value(DEFAULT_VAL)
	public AWSCredentials getAWSCredentials(String provider) {
		AWSCredentials credentials = null;
		logger.info("Initializing AWS Credentials ...");
		try {
			credentials = new ProfileCredentialsProvider(provider != null ? provider : DEFAULT_PROVIDER)
					.getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (<users-folder>\\.aws\\credentials), and is in valid format.", e);
		}
		logger.info("AWS Credentials Initialized Successfully ...");
		return credentials;
	}

}
