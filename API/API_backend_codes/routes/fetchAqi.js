
exports = module.exports = function (app, mongoose) {

    var express = require('express');
    var router = express.Router();

    /* GET users listing. */
    router.post('/getall', async function (req, res) {
        try {
            const AQIModelModel = app.db.models.AQIModel;
            let dataResponse = await AQIModelModel.find({});
            res.status(200).send({ success: true, data: dataResponse });
        } catch (err) {
            res.status(500).send(err);
        }
    });

    router.post('/', async function (req, res) {
        try {
            if (!req.body.city) {
                return res.status(400).send({ message: "Please provide city on body" })
            }
            if (!req.body.date) {
                return res.status(400).send({ message: "Please provide city on body" })
            }
            const AQIModelModel = app.db.models.AQIModel;
            let dataResponse = await AQIModelModel.find({ city: req.body.city });
            let afterJsonData = JSON.parse(JSON.stringify(dataResponse));
            let datatoSend = afterJsonData.find(data => data.date.utc.split('T')[0] == req.body.date);
            console.log(datatoSend);
            if (!datatoSend) {
                return res.status(200).send({ message:"No record found" });
            }
            // console.log("after slice ==>>", afterslice);
            res.status(200).send({ AQI_value:datatoSend.value });
        } catch (err) {
            res.status(500).send(err);
        }
    });


    app.use('/fetch_aqi', router);
};
