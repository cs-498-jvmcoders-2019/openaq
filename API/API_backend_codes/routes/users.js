# not required anymore 
exports = module.exports = function (app, mongoose) {

  var express = require('express');
  var router = express.Router();

  /* GET users listing. */
  router.post('/register', async function (req, res, next) {
    try {

      let body = req.body;
      if (!body.username) {
        return res.status(400).send({ message: "Please provide username in req body " });
      }
      if (!body.email) {
        return res.status(400).send({ message: "Please provide email in req body " });
      }
      if (!body.password) {
        return res.status(400).send({ message: "Please provide password in req body " });
      }
      if (!body.mobile_number) {
        return res.status(400).send({ message: "Please provide mobileNumber in req body " });
      }
      if (!body.city) {
        return res.status(400).send({ message: "Please provide mobileNumber in req body " });
      }
      let User = new app.db.models.User({
        email: body.email,
        password: body.password,
        username: body.username,
        mobile_number: body.mobile_number,
        city: body.city
      });
      let savedUser = await User.save();
      res.status(200).json({ user_id: savedUser._id, aqi_value: 0 });
    } catch (err) {
      res.status(500).send(err);
    }
  });

  app.use('/user', router);
};
