
exports = module.exports = function (app, mongoose) {

    var express = require('express');
    var router = express.Router();

    /* GET users listing. */

    router.post('/fetch_aqi', async function (req, res, next) {
        try {
            if (!req.body.city_name) {
                return res.status(400).json({ message: "Please provide city in req body" })
            }
            if (!req.body.date) {
                return res.status(400).json({ message: "Please provide date in req body" })
            }
            //need to get api here
            // res.status(200).json(savedRepose);
        } catch (err) {
            res.status(500).send(err);
        }
    });



    router.post('/hi_data_entry', async function (req, res, next) {
        try {
            if (!req.body.city) {
                return res.status(400).json({ message: "Please provide city in req body" })
            }
            if (!req.body.date) {
                return res.status(400).json({ message: "Please provide date in req body" })
            }
            if (!req.body.health_index) {
                return res.status(400).json({ message: "Please provide health in req body" })
            }
            if (req.body.health_index > 10 || req.body.health_index < 1) {
                return res.status(400).json({ message: "Please provide a valid health index in req body" })
            }
            let dateData = req.body.date.split('-');
            try {
                checkDatelength(dateData);
            } catch (err) {
                console.log(err.message);
                return res.status(400).send({ message: err.message })
            }
            console.log(dateData)
            let UserHealthResponseModel = new app.db.models.UserHealthResponse({ city: req.body.city, date: req.body.date, health_index: req.body.health_index });
            let savedRepose = await UserHealthResponseModel.save();
            res.status(200).json({ message: "Response Saved" });
        } catch (err) {
            res.status(500).send(err);
        }
    });


    router.post('/get_usercount_withHealthissue', async function (req, res, next) {
        try {
            if (!req.body.city) {
                return res.status(400).json({ message: "Please provide city in req body" })
            }
            if (!req.body.date) {
                return res.status(400).json({ message: "Please provide date in req body" })
            }
            let dateData = req.body.date.split('-');
            try {
                checkDatelength(dateData);
            } catch (err) {
                console.log(err.message);
                return res.status(400).send({ message: err.message })
            }
            const UserHealthResponseModel = app.db.models.UserHealthResponse;
            let findedRespose = await UserHealthResponseModel.find({ city: req.body.city, date: req.body.date, health_index: { $lt: 8 } }).count();
            res.status(200).json({ num_users: findedRespose });
        } catch (err) {
            res.status(500).send(err);
        }
    });

    function checkDatelength(date) {
        if (date.length < 3 || date.length > 3) {
            throw Error("Please provide date correct")
        }
        if (date[0].length < 4 || date[0].length > 4) {
            throw Error("Please provide  correct  year format");
        }
        if (date[1].length < 2 || date[1].length > 2) {
            throw Error("Please provide  correct  month format");
        }
        if (date[2].length < 2 || date[2].length > 2) {
            throw Error("Please provide  correct  day format");
        }
    }

    app.use('/api', router);
};
