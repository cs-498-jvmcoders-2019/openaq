#not needed now
exports = module.exports = function (app, mongoose) {
    'use strict';
    let Schema = mongoose.Schema;

    let UserSchema = new Schema({
        username: {
            type: String,
        },
        email: {
            type: String,
        },
        password: {
            type: String
        },
        mobile_number: {
            type: String
        },
        city: {
            type: String
        }
    });
    app.db.model('User', UserSchema);
}