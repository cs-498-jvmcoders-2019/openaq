exports = module.exports = function (app, mongoose) {
    require('./user')(app, mongoose);
    require('./UserAQIResponse')(app, mongoose);
    require('./AQIModel')(app, mongoose);
}