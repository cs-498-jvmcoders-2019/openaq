exports = module.exports = function (app, mongoose) {
    'use strict';
    let Schema = mongoose.Schema;

    let data = new Schema({
        utc: { type: Date },
        local: { type: String }
    });
    let averagingPeriod = new Schema({
        unit: String,
        value: Number
    })

    let coordinates = new Schema({
        latitude: String,
        longitude: String
    });

    let attribution = new Schema({
        name: String,
        url: String
    })

    let AQISChema = new Schema({
        date: {
            type: data,
        },
        parameter: {
            type: String,
        },
        value: {
            type: Number
        },
        unit: {
            type: String
        },
        averagingPeriod: {
            type: averagingPeriod
        },
        location: {
            type: String
        },
        city: {
            type: String
        },
        country: {
            type: String
        },
        coordinates: {
            type: coordinates
        },
        attribution: {
            type: [attribution]
        },
        sourceName: {
            type: String
        },
        sourceType: {
            type: String
        },
        mobile: {
            type: Boolean
        }
    });
    app.db.model('AQIModel', AQISChema);
}