exports = module.exports = function (app, mongoose) {
    'use strict';
    let Schema = mongoose.Schema;

    let UserHealthResponse = new Schema({
        city: {
            type: String,
        },
        health_index: {
            type: Number,
        },
        date: {
            type: String
        }
    });
    app.db.model('UserHealthResponse', UserHealthResponse);
}