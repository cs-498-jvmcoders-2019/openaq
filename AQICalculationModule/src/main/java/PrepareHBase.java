import net.minidev.json.JSONObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.HashMap;
import java.util.Map;

public class PrepareHBase {
    //public static String remoteHost = "192.168.99.100";
//    public static String remoteHost = "hbasehost";

    //For local Testing purposes, a simple HashMap to store the key and vals
    private Map<String, JSONObject> dataHolder = new HashMap<>();
    private HTable aqiTable;

    public static String colFamily = "details";

    public static void main(String[] args) {
        try {
            PrepareHBase hb = new PrepareHBase("192.168.99.100");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public PrepareHBase(String hbaseHost) throws Exception {
        //      Instantiating   configuration   class


        Configuration con = HBaseConfiguration.create();
//        con.setInt("timeout", 1200);
        con.set("hbase.zookeeper.quorum", hbaseHost);
        con.set("hbase.zookeeper.property.clientPort", "2181");
        con.set("hbase.master", hbaseHost + ":60000");


        try {
            HBaseAdmin.checkHBaseAvailable(con);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //      Instantiating   HbaseAdmin      class
        HBaseAdmin admin = new HBaseAdmin(con);
        TableName tableName = TableName.valueOf("aqiTable");
        if (!admin.tableExists(tableName)) {
            //      Instantiating   table   descriptor      class
            HTableDescriptor tableDescriptor1 = new HTableDescriptor(tableName);

            //      Adding  column  families        to      table   descriptor
            tableDescriptor1.addFamily(new HColumnDescriptor(colFamily));
            //      Execute the     table   through admin

            admin.createTable(tableDescriptor1);
        }
        aqiTable = new HTable(con, tableName);

    }


    /**
     * Given a key (timestamp+city+country) , return the AQI representing this key
     * @param key
     * @return
     */
    public int getAQI(String key) {

            try {
//	Instantiating	Get	class
                Get g = new Get(Bytes.toBytes(key));

                //	Reading	the	data
                Result result = aqiTable.get(g);
                if(result != null) {
                    //	Reading	values	from	Result	class	object
                    byte[] value = result.getValue(Bytes.toBytes(colFamily), Bytes.toBytes(CalculateAQIMainKafka.AQI));
                    if(value != null) {
                        return Integer.parseInt(Bytes.toString(value));
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
//        else {
//            JSONObject data = dataHolder.get(key);
//            if(data != null) {
//                String val = data.getAsString(CalculateAQIMainKafka.AQI);
//                if(val != null) {
//                    return Integer.parseInt(val);
//                }
//            }
//        }
        return -1;
    }

    /**
     * Given a key (timestamp+city+country) , return the JSONObject representing this key
     * @param key
     * @return
     */
    public void saveObject(String key, JSONObject obj) {
//        if (System.getProperty("WINDOWS") == null) {
            try {
                //	Instantiating	Put	class							//	accepts	a	row	name.

                final Put p = new Put(Bytes.toBytes(key));

                byte[] colFamilyName = Bytes.toBytes(colFamily);

                obj.entrySet().forEach(entry-> {
                    String entryKey = entry.getKey();
                    Object entryVal = entry.getValue();
                    //	adding	values	using	add()	method							//	accepts	column	family	name,	qualifier/row	name	,value
                    if (entryVal != null) {
                        String data = entryVal.toString();
                        p.addColumn(colFamilyName, Bytes.toBytes(entryKey), Bytes.toBytes(data));
                    }
                });

                //	Saving	the	put	Instance	to	the	HTable.
                aqiTable.put(p);


            } catch (Exception e) {
                e.printStackTrace();
            }
//        } else {
//            dataHolder.put(key, obj);
//        }
    }

    public void print() {
        for(Map.Entry<String,JSONObject> entry : dataHolder.entrySet()) {
            System.out.println(entry.getKey() +":"+ entry.getValue().getAsNumber(CalculateAQIMainKafka.AQI));
        }
    }
}
