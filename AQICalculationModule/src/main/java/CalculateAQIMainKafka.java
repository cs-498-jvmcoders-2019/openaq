import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

import java.util.regex.Pattern;

import kafka.serializer.StringDecoder;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;
import net.minidev.json.parser.ParseException;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.kafka.KafkaUtils;
import scala.Tuple2;



import org.apache.kafka.clients.consumer.ConsumerConfig;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import org.apache.kafka.common.serialization.StringDeserializer;



import org.apache.spark.SparkConf;

import org.apache.spark.streaming.api.java.*;



import org.apache.spark.streaming.Durations;



/**

 * Consumes messages from one or more topics in Kafka and calculates the AQI.

 * Usage: CalculateAQIMainKafka <brokers> <groupId> <topics>

 *   <brokers> is a list of one or more Kafka brokers

 *   <topics> is a list of one or more kafka topics to consume from

 *

 *

 */



public final class CalculateAQIMainKafka {
    static String O3 = "O3";
    static String NO2 = "NO2";
    static String SO2 = "SO2";
    static String CO = "CO";
    static String PM25 = "PM25";
    static String PM10 = "PM10";
    static String AQI = "aqi";

    static Map<String,Map<String,String>> breakPointsTable = new HashMap<>();

    private static final Pattern SPACE = Pattern.compile(" ");
    static JavaPairInputDStream<String, String> messages;
    static JavaStreamingContext jssc;

    static String resultsDir;
    static PrepareHBase hbase;
    void initialize(String brokers, String hbaseHost, String topics, String offset) {
        if (System.getProperty("WINDOWS") != null) {
            System.setProperty("hadoop.home.dir", "C:\\winutils");


        }
        try {
            hbase = new PrepareHBase(hbaseHost);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create context with a 2 seconds batch interval
        SparkConf sparkConf = new SparkConf().setMaster("local").setAppName("AQI Calculator");

        jssc = new JavaStreamingContext(sparkConf, new Duration(30000));


        Set<String> topicsSet = new HashSet<>(Arrays.asList(topics.split(",")));

        Map<String, String> kafkaParams = new HashMap<>();

        kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);

        kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG, "CalculateAQI");
        kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offset);
        kafkaParams.put("fetch.message.max.bytes", "60000000");
        //fetch.message.max.bytes


        resultsDir = "." + File.separator + "AQIData";


//        kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//
//        kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

        // Create direct kafka stream with brokers and topics
        messages = KafkaUtils.createDirectStream(jssc,
                String.class, String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topicsSet);
// Start the computation
        processMessages();
        jssc.start();

        try {
            jssc.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

        if (args.length < 3) {

            System.err.println("Usage: JavaDirectKafkaWordCount <brokers> <hbase_host> <topics>\n" +

                    "  <brokers> is a list of one or more Kafka brokers\n" +

                    "  <hbase_host> is the IP address of the host where Hbase is running\n" +

                    "  <topics> is a list of one or more kafka topics to consume from\n\n");

            System.exit(1);

        }


        String brokers = args[0];
        String hbaseHost = args[1];
        String topics = args[2];
        //by default the application starts from the latest offset - for testing purpose, we can start from the beginning
        //smallest, largest
        String offset = "largest";
        if(args.length >= 4) {
         offset = args[3];
        }

        readInBreakPointsTable();
        CalculateAQIMainKafka mainClass = new CalculateAQIMainKafka();
        mainClass.initialize(brokers,hbaseHost,topics,offset);

    }

    private static void readInBreakPointsTable() throws Exception {
        //Read in the breakpoints.csv used to calculate the AQI
        List<String> allLines = null;

        String respath = "breakPoints.csv";
        URL in = CalculateAQIMainKafka.class.getResource(respath);
        if ( in == null ) {
            throw new Exception("resource not found: " + respath);
        }
        allLines = Files.readAllLines(Paths.get(in.toURI()));
        int first = 0;

        String[] headings = null;
        for(String line : allLines) {
            if(first == 0) {
                //first line is the headings
                //O3-8,O3-1,PM25-1,PM10-1,CO-1,SO2-1,NO2-1,AQI
                headings = line.split(",");
                first++;
                continue;
            }
            String parts[] = line.split(",");
            int aqiValHeading= 0;
            Map<String,String> colMap = new HashMap<>();
            for(int i = 2 ; i < parts.length; i++) {
                String heading = headings[i].toUpperCase();
                colMap = breakPointsTable.get(heading) ;
                if(colMap == null) {
                    colMap = new HashMap<>();
                    breakPointsTable.put(heading,colMap);
                    String[] paramParts = heading.split("-");

                    //for pm10 and pm25, add 24 hour sampling also
                    if(heading.startsWith("PM10") || heading.startsWith("PM25") || heading.startsWith("SO2")) {
                        breakPointsTable.put(paramParts[0]+"-24",colMap);
                    }
                    if(heading.startsWith("CO")) {
                        breakPointsTable.put(paramParts[0]+"-8",colMap);
                    }
                }
                colMap.put(parts[i],parts[0]);


            }

        }
    }

    private static void processMessages() {
        // Get each input record and calculate the AQI
        messages.foreachRDD(rdd -> {
            System.out.println("--- New RDD with " + rdd.partitions().size()
                    + " partitions and " + rdd.count() + " records");
            JavaPairRDD<String, JSONObject> aggData = processRecord(rdd);
            //Calculate the AQI on the data that has been aggregated by timestamp+city+country
            //        Now run the actual calculation of AQI on the aggregated data
            //        Now run the actual calculation of AQI on the aggregated data
            JavaPairRDD<String, JSONObject> aqiData = aggData.mapValues(new Function<JSONObject, JSONObject>() {


                @Override
                public JSONObject call(JSONObject v1) throws Exception {
                    JSONObject jsonRet = new JSONObject(v1);
                    calculateAQI(jsonRet);
                    return jsonRet;
                }
            });
            Map<String, JSONObject> calculatedMap = aqiData.collectAsMap();

            //Save to HBase
            for(Map.Entry<String,JSONObject> entry : calculatedMap.entrySet()) {
//                System.out.println("*******" + entry.getKey()+":"+entry.getValue().toJSONString());
                String key = entry.getKey();
                JSONObject jsonObj = entry.getValue();
                String aqiStr = jsonObj.getAsString(AQI);
                int aqiVal = -1;
                if(aqiStr != null) {
                    aqiVal = Integer.parseInt(aqiStr);
                }
                int savedAQI = hbase.getAQI(key);
                if(aqiVal != -1 && (savedAQI == -1 || aqiVal > savedAQI)) {
                    //Nothing saved yet, or newly calculated value for the same timestamp+city+country is larger
                    //than saved value
                    hbase.saveObject(key,jsonObj);
                }

            }

        });

        //No computation is invoked unless one of the following is invoked:
//        print()
//        foreachRDD(func)
//        saveAsObjectFiles(prefix, [suffix])
//        saveAsTextFiles(prefix, [suffix])
//        saveAsHadoopFiles(prefix, [suffix])
    }

    //This RDD contains Key and Value of each record
    private static JavaPairRDD<String, JSONObject>  processRecord(JavaPairRDD<String, String> record) {
        JavaPairRDD<String, JSONObject> aggData = record.mapToPair((PairFunction<Tuple2<String, String>, String, JSONObject>) kafkaRec -> {
            //create a json object from the json string

            try {
                String[] records = kafkaRec._2().split("\n");
                for(String oneRec : records) {
                    if(oneRec.isEmpty()) {
                        continue;
                    }
                    JSONObject json = (JSONObject) JSONValue.parseStrict(oneRec);
                    if(json == null) {
                        continue;
                    }
                    JSONObject dateObj = (JSONObject) JSONValue.parseStrict(json.getAsString("date"));
                    //For the timestamp part of the key - use just the date part from: 2019-03-16T07:00:00.000Z
                    String utcStr = dateObj.getAsString("utc");
                    int idx = utcStr.lastIndexOf("-");
                    utcStr = utcStr.substring(0, idx + 3);
                    String key = utcStr + ":" + json.getAsString("country") + ":" + json.getAsString("city");
                    key = key.toLowerCase();
                    JSONObject jsonRet = new JSONObject();
                    //append the "averagingPeriod":{"unit":"hours","value":1} to the parameter - since some parameters like 03 are reported with 8 hour avg or 1 hour avg
                    //and we need to differentiate between the two
                    String avgPeriodStr = json.getAsString("averagingPeriod");
                    String paramKey = json.getAsString("parameter") + "-1";
                    if(avgPeriodStr != null) {
                        JSONObject avgPeriod = (JSONObject) JSONValue.parseStrict(json.getAsString("averagingPeriod"));
                        paramKey = json.getAsString("parameter") + "-" + avgPeriod.get("value");
                    }
                    paramKey = paramKey.toUpperCase();
                    jsonRet.put(paramKey, json.getAsString("value"));
                    return new Tuple2<String, JSONObject>(key, jsonRet);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }).reduceByKey((Function2<JSONObject, JSONObject, JSONObject>) (v1, v2) -> {
            JSONObject jsonRet = new JSONObject(v1);
            for(Map.Entry<String,Object> entry : v2.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if(jsonRet.containsKey(key)) {
                    //if the two keys are same, take the max of the values

                    Float val1 = Float.parseFloat(jsonRet.getAsString(key));
                    Float val2 = Float.parseFloat(value.toString());
                    Float maxVal = val1 > val2 ? val1 : val2;
                    value = maxVal;
                }
                jsonRet.put(key,value.toString());

            }
            return jsonRet;
        });
        return aggData;
    }

    /**
     * Calculate the AQI with the available parameters and store it in the field named "aqi" . It is an Integer ranging from 0-100
     * @param jsonRet
     */
    private static void calculateAQI(JSONObject jsonRet) {
        //calculate the aqi
        /**
         * Ozone (ppm) – truncate to 3 decimal places
         * PM2.5 (µg/m3) – truncate to 1 decimal place
         * PM10 (µg/m3) – truncate to integer
         * CO (ppm) – truncate to 1 decimal place
         * SO2 (ppb) – truncate to integer
         * NO2 (ppb) – truncate to integer
         */
        DecimalFormat df = new DecimalFormat("####.#");
        DecimalFormat df3 = new DecimalFormat("####.###");
        df.setRoundingMode(RoundingMode.DOWN);
        int aqiCalcVal = 0;
        for(Map.Entry<String,Object> jentry : jsonRet.entrySet()) {
            String key = jentry.getKey();
            if(key.startsWith(O3) || key.startsWith(CO) || key.startsWith(NO2) || key.startsWith(PM10) || key.startsWith(PM25) || key.startsWith(SO2))
            {
                Float fVal = Float.parseFloat(df.format(Float.parseFloat(jentry.getValue().toString())));

                if(key.startsWith(O3)) {
                    fVal = Float.parseFloat(df3.format(Float.parseFloat(jentry.getValue().toString())));
                }

                if(key.startsWith(PM10) || key.startsWith(SO2) || key.startsWith(NO2)) {
                    fVal = Float.parseFloat(jentry.getValue().toString());
                    fVal = (float)fVal.intValue();
                }

                Map<String, String> secMap = breakPointsTable.get(key);
                String aqiVal = null;
                Float BpHi = null, BpLo = null;
                for (Map.Entry<String, String> entry : secMap.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase("NA")) {
                        continue;
                    }
                    String[] parts = entry.getKey().split("-");
                    BpLo = Float.parseFloat(parts[0]);
                    BpHi = Float.parseFloat(parts[1]);

                    if (fVal >= BpLo && fVal <= BpHi) {
                        //Found the aqi value for this parameter
                        aqiVal = entry.getValue();
                        break;
                    }
                }
                if (aqiVal != null) {
                    //use the formula:
                    /**
                     * Equation 1:
                     *
                     * Ip = (IHi - ILo)/(BpHi-BpLo) (Cp-BpLo) +ILo
                     *
                     * Where Ip = the index for pollutant p
                     * Cp = the truncated concentration of pollutant p
                     * BPHi = the concentration breakpoint that is greater than or equal to Cp
                     * BPLo = the concentration breakpoint that is less than or equal to Cp
                     * IHi = the AQI value corresponding to BPHi
                     * ILo = the AQI value corresponding to BPLo
                     */
                    String[] parts = aqiVal.split("-");
                    Float IpLo = Float.parseFloat(parts[0]);
                    Float IpHi = Float.parseFloat(parts[1]);

                    Float ip = (IpHi - IpLo) / (BpHi - BpLo) * (fVal - BpLo) + IpLo;
                    if (ip > aqiCalcVal) {
                        //use the max index calculated so far
                        aqiCalcVal = Math.round(ip);
                    }

                }
            }
        }

        jsonRet.put(AQI, aqiCalcVal);
    }
}