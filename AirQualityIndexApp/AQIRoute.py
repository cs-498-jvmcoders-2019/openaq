from flask import Flask, render_template, request, redirect, send_from_directory, url_for, send_file
app=Flask(__name__)


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html')


@app.route("/about")
def about():
    return render_template('about.html')

 

@app.route("/AQIHealthImpact")
def impact():
    return render_template('aqihealthimpact.html')

@app.route("/userinput")
def input():
    return render_template('userinput.html')         


   


@app.route('/login', methods=['GET','POST'])
def login():
	if (request.method == 'POST'):
		username = request.form['uname']
		password = request.form['psw']
		if (res == "SUCCESS"):
			return render_template('userinput.html')
		else:
			error_msg = 'Please enter a valid username and/or password'
			data = { 'error': error_msg }
			return render_template('Login.html', data=data)
	
	return render_template('Login.html')

if __name__=='__main__':
    app.run(debug=True)     
